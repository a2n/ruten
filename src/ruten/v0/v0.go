package v0

import (
	"log"
	"bytes"
	"reflect"
	"runtime"
	"time"
	"os"
	"fmt"
	"net/http"
	"strings"
	"io"
	"io/ioutil"
	"net/url"

    iconv "github.com/djimenez/iconv-go"
    "code.google.com/p/go.net/html"

	"database/sql"
	_ "github.com/go-sql-driver/mysql"
)

func GetCurrentMethodName(i interface{}) string {
	return runtime.FuncForPC(reflect.ValueOf(i).Pointer()).Name()
}

type Service struct {
	UserAgent string
	Cookie *http.Cookie
}

func NewService() *Service {
	// Log
	const path = "ruten.log"
	file, err := os.OpenFile(path, os.O_RDWR | os.O_APPEND | os.O_CREATE , 0666)
	if err != nil {
		log.Fatal(err)
	}
	log.SetOutput(file)

	// Ruten cookie
	cookie := &http.Cookie {
		Name: "_ts_id",
		Value: "3D053C043C0C3A0C3F",
		Path: "/",
		Domain: "ruten.com.tw",
		Expires: time.Now().AddDate(1, 0, 0),
	}

	return &Service {
		UserAgent: "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.91 Safari/537.36",
		Cookie: cookie,
	}
}

type Category struct {
	Service *Service
	Id string
	ParentId string
	Title string
	InsertDate time.Time
	Enable bool
}

func NewCategory(s *Service) *Category {
    return &Category {
		Service: s,
    }
}

func (c *Category) DownloadRecursive(id string) int {
	result := make([]Category, 0)

	var f func(string)
	f = func(id string) {
		r, err := c.DownloadId(id)
		if err != nil {
			log.Fatal(err)
		}
		slice := c.Parse(r)
		result = append(result, slice...)

		for _, c := range slice {
			fmt.Printf("%s (%s)\n", c.Title, c.Id)
			c.Save()
			if len(c.Id) > 4 {
				f(c.Id)
			}
		}
	}
	f(id)

	return len(result)
}

func (c *Category) DownloadId(id string) (io.Reader, error) {
	if len(id) == 0 {
		err := fmt.Errorf("%s has error, empty id.", GetCurrentMethodName(c.DownloadId))
		log.Printf(err.Error())
		return nil, err
	}
	c.Id = id

	url := bytes.NewBufferString("http://class.ruten.com.tw/category/")
	if id != "0" {
		if len(id) == 4 {
			// Top level
			url.WriteString("main?")
		} else if len(id) > 4{
			// Non-top level
			url.WriteString("sub00.php?c=")
		}
		url.WriteString(id)
	}
	log.Printf("Download %s\n", url.String())

    req, err := http.NewRequest("GET", url.String(), nil)
    if err != nil {
		err = fmt.Errorf("%s has error, %s.", GetCurrentMethodName(c.DownloadId), err.Error())
		log.Printf(err.Error())
		return nil, err
    }
    req.Header.Set("User-Agent", c.Service.UserAgent)
	req.AddCookie(c.Service.Cookie)

    resp, err := http.DefaultClient.Do(req)
    if err != nil {
		err = fmt.Errorf("%s has error, %s.", GetCurrentMethodName(c.DownloadId), err.Error())
		log.Printf(err.Error())
		return nil, err
    }

	b, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		err = fmt.Errorf("%s has error, %s.", GetCurrentMethodName(c.DownloadId), err.Error())
		log.Printf(err.Error())
		return nil, err
	}

	str, err := iconv.ConvertString(string(b), "BIG-5", "UTF8//IGNORE")
	if err != nil {
		err = fmt.Errorf("%s has error, %s.", GetCurrentMethodName(c.DownloadId), err.Error())
		log.Printf(err.Error())
		return nil, err
	}

	return strings.NewReader(str), nil
}

func (c *Category) TestParse() {
    // Testing condition
    file, err := os.Open("examples/c8")
    if err != nil {
		log.Fatal(err)
    }

	c.Parse(file)
}

func (c *Category) Parse(reader io.Reader) []Category {
    // html parser
    node, err := html.Parse(reader)
    if err != nil {
		err = fmt.Errorf("%s has error, %s\n", GetCurrentMethodName(c.Parse), err.Error())
		log.Printf(err.Error())
		return nil
    }

	slice := make([]Category, 0)

    var f func(*html.Node, *string)
    f = func(n *html.Node, parentId *string) {
		if n.Type == html.ElementNode && n.Data == "span" && len(n.Attr) > 0 {
			a := n.Attr[0]

			// Level 0
			if a.Key == "class" && a.Val == "be16" {
				node := n.FirstChild

				// Id
				url, err := url.Parse(node.Attr[0].Val)
				if err != nil {
					err = fmt.Errorf("%s has error, %s\n", GetCurrentMethodName(c.Parse), err.Error())
					log.Printf(err.Error())
					return
				}
				id := ""
				for k, _ := range url.Query() {
					id = k
				}

				// Title
				name := strings.TrimSpace(node.FirstChild.Data)

				*parentId = "0"
				category := Category {
					Id: id,
					ParentId: *parentId,
					Title: name,
				}
				slice = append(slice, category)
				*parentId = category.Id
			}

			// Level 1
			if a.Key == "class" && a.Val == "bk13" {
				node := n.FirstChild.NextSibling

				// Id
				url, err := url.Parse(node.Attr[0].Val)
				if err != nil {
					err = fmt.Errorf("%s has error, %s\n", GetCurrentMethodName(c.Parse), err.Error())
					log.Printf(err.Error())
					return
				}
				id := ""
				for k, _ := range url.Query() {
					id = k
				}

				// Title
				name := strings.TrimSpace(node.FirstChild.Data)

				category := Category {
					Id: id,
					ParentId: *parentId,
					Title: name,
				}
				slice = append(slice, category)
			}
		}

		// Level 2 and more
		if n.Type == html.ElementNode && n.Data == "h2" && len(n.Attr) > 0 {
			a := n.Attr[0]

			if a.Key == "class" && a.Val == "title" {
				node := n.FirstChild
				if node.Data != "a" {
					return
				}

				// Id
				url, err := url.Parse(node.Attr[0].Val)
				if err != nil {
					err = fmt.Errorf("%s has error, %s\n", GetCurrentMethodName(c.Parse), err.Error())
					log.Printf(err.Error())
					return
				}
				id := url.Query()["c"][0]

				// Title
				name := strings.TrimSpace(node.FirstChild.Data)

				*parentId = id[0:len(id) - 4]
				category := Category {
					Id: id,
					ParentId: *parentId,
					Title: name,
				}
				slice = append(slice, category)
			}
		}

		for c := n.FirstChild; c != nil; c = c.NextSibling {
			f(c, parentId)
		}
    }

	id := "0"
    f(node, &id)

	return slice
}

func (c *Category) Save() {
	// MySQL connection
	db, err := sql.Open("mysql", "root:testtest@tcp(127.0.0.1:3306)/ruten")
	if err != nil {
		log.Fatal(err)
	}

	str := fmt.Sprintf("INSERT INTO Category (Id, ParentId, Title, InsertDate, Enable) VALUES ('%s', '0', '%s', NOW(), true);", c.Id, c.Title);
	_, err = db.Exec(str)
	if err != nil {
		err = fmt.Errorf("%s has error, %s", GetCurrentMethodName(c.Save), err.Error())
		log.Printf(err.Error())
	}

	db.Close()
}
